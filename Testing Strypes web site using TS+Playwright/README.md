# Strypes 

## Getting Started

## Installation process

1. Open Terminal into the projects directory in order to install its dependencies

   ```bash
   npm install
   #Add dependency and install browsers.
   npm i -D @playwright/test
   #install supported browsers
   npx playwright install
   ```
   
2. Create .env file and put the PLAYWRIGHT_BASE_URL= inside
   - `.env`

## Available scripts

```bash
# run tests with tag
npm run test:regression

# format code with Prettier
npm run format

# Open the interactive UI to run the tests
npm run test:regression:ui

```

## Folder Structure

```bash
.

├── tests                   # End to end/Regression tests
│   ├── strypes
│       ├──configs
│       ├── constants
│       ├── fixtures
│       ├── pages
│       ├── specs
│       └── utils
├── .env                  # Default `dotenv` secrets
├── .gitignore
├── package-lock.json
├── package.json
├── README.md
```
