import BasePage from "./base-page";
import { Locator } from "@playwright/test";
import CONSTANTS from "../constants/constants";

export default class JobPage extends BasePage {
  readonly jobTitle: Locator;

  readonly companyInfo: Locator;

  readonly jobDescriptionSection: Locator;

  readonly applyButton: Locator;

  readonly compensationSection: Locator;
  constructor(page, relativeUrl) {
    super(page, relativeUrl);

    this.jobTitle = page.getByRole("heading", {
      name: CONSTANTS.JOB_CONSTANTS.qa_position_name,
    });
    this.companyInfo = page.getByText("COMPANY INFORMATION ICT");
    this.jobDescriptionSection = page
      .locator("section")
      .filter({ hasText: "We are looking for a seasoned" });
    this.applyButton = page.getByRole("link", { name: "Apply now" });
    this.compensationSection = page
      .locator("section")
      .filter({ hasText: "COMPENSATION BENEFITS" });
  }
}
