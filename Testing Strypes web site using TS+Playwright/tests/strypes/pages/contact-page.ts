import BasePage from "./base-page";
import { Locator } from "@playwright/test";
import {
  clearFieldAndEnterSequentially,
  generateRandomEmail,
  generateRandomName,
} from "../utils/common";

export default class ContactPage extends BasePage {
  readonly firstName: Locator;
  readonly lastName: Locator;
  readonly email: Locator;
  readonly companyName: Locator;
  readonly message: Locator;
  readonly termsButton: Locator;
  readonly sendButton: Locator;
  readonly thankYouMessage: Locator;

  constructor(page, relativeUrl) {
    super(page, relativeUrl);
    this.firstName = this.page.getByPlaceholder("First name:");
    this.lastName = this.page.getByPlaceholder("Last name:");
    this.email = this.page.getByPlaceholder("E-mail:*");
    this.companyName = this.page.getByPlaceholder("Company name:");
    this.message = this.page.getByPlaceholder("Messages:");
    this.termsButton = this.page.getByLabel(
      "I agree with the Privacy policy and Terms of use.*"
    );
    this.sendButton = this.page.getByRole("button", { name: "SEND" });
    this.thankYouMessage = this.page.getByText("Thank you for submitting the");
  }

  async enterFirstName() {
    await clearFieldAndEnterSequentially(this.firstName, await generateRandomName());
  }
  async enterLastName() {
    await clearFieldAndEnterSequentially(this.lastName, await generateRandomName());
  }

  async enterEmail() {
    await clearFieldAndEnterSequentially(this.email, await generateRandomEmail());
  }

  async enterCompanyName() {
    await clearFieldAndEnterSequentially(this.companyName, await generateRandomName());
  }

  async enterMessage() {
    await clearFieldAndEnterSequentially(this.message, "test");
  }

  async fillAllGetInTouchFields() {
    await this.enterFirstName();
    await this.enterLastName();
    await this.enterEmail();
    await this.enterCompanyName();
    await this.enterMessage();
  }

  async clickTermsOfUseButton() {
    await this.termsButton.click();
  }
  async clickSendButton() {
    await this.sendButton.click();
  }
}
