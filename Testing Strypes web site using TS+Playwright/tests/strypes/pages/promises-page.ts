import BasePage from "./base-page";
import { Locator } from "@playwright/test";

export default class PromisesPage extends BasePage {
  readonly headingTitle: Locator;

  constructor(page, relativeUrl) {
    super(page, relativeUrl);
    this.headingTitle = page.getByRole("heading", { name: "Our promises" });
  }

  async getHeadingTitle() {
    return this.headingTitle.innerText();
  }
}
