import BasePage from "./base-page";
import { Locator } from "@playwright/test";

export default class CareerPage extends BasePage {
  readonly dropDown: Locator;
  readonly headingTitle: Locator;
  readonly headingDescription: Locator;
  readonly greatPlaceToWorkDescription: Locator;
  readonly challengeContentTitle: Locator;
  readonly challengeContentDescription: Locator;
  readonly hireTitle: Locator;
  readonly hireSubTitle: Locator;
  readonly jobSection: Locator;
  readonly ourTeamTitle: Locator;
  readonly ourTeamDescription: Locator;
  readonly watchVideoButton: Locator;
  readonly awesomeSection: Locator;
  readonly locationSection: Locator;
  readonly internshipTitle: Locator;
  readonly internshipDescription: Locator;
  readonly internshipWatchVideoButton: Locator;
  readonly startCareerTitle: Locator;
  readonly startCareerDescription: Locator;
  readonly strypesLabButton: Locator;

  constructor(page, relativeUrl) {
    super(page, relativeUrl);
    this.dropDown = this.page.getByRole("combobox");
    this.headingTitle = this.page.getByRole("heading", {
      name: "We are ICT Strypes - we do",
    });
    this.headingDescription = this.page.getByText("We want our team to feel");
    this.greatPlaceToWorkDescription = this.page.getByRole("heading", {
      name: "We are officially recognized",
    });
    this.challengeContentTitle = this.page.getByRole("heading", {
      name: "Take on our Challenge!",
    });
    this.challengeContentDescription = this.page.getByRole("heading", {
      name: "Grab the opportunity to win",
    });
    this.hireTitle = this.page.getByRole("heading", { name: "We are hiring" });
    this.hireSubTitle = this.page.getByRole("heading", { name: "Search open positions" });
    this.jobSection = this.page.locator("section").filter({
      hasText:
        "Automation Quality Assurance Engineer View C/C++ Developer View Data Analyst",
    });
    this.ourTeamTitle = this.page.getByRole("heading", {
      name: "Our awesome team",
      exact: true,
    });
    this.ourTeamDescription = this.page.getByText("What makes ICT Strypes");
    this.watchVideoButton = this.page.getByRole("link", { name: "Watch video" }).first();
    this.awesomeSection = this.page
      .locator("section")
      .filter({ hasText: "We would like to make you" });
    this.locationSection = this.page
      .locator("section")
      .filter({ hasText: "Locations Sofia Plovdiv" });
    this.internshipTitle = this.page.getByRole("heading", {
      name: "Internship at ICT Strypes",
    });
    this.internshipDescription = this.page.getByText("At ICT Strypes we are");
    this.internshipWatchVideoButton = this.page
      .locator("section")
      .filter({ hasText: "Internship at ICT Strypes At" })
      .getByRole("link");
    this.startCareerTitle = this.page.getByRole("heading", {
      name: "Do you think about starting a",
    });
    this.startCareerDescription = this.page
      .locator("p")
      .filter({ hasText: "Strypes Lab 2023 is our main" });
    this.strypesLabButton = this.page.getByRole("link", { name: "Strypes Lab" });
  }

  async chooseOpenPositionFromDropDown(positionName: string) {
    await this.dropDown.selectOption({ label: `${positionName}` });
  }

  async clickPositionName(positionName: string) {
    await this.page.getByRole("link", { name: positionName }).click();
  }
}
