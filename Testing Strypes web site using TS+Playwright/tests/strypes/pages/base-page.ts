import { Page } from "@playwright/test";

export default abstract class BasePage {
  readonly page: Page;

  protected readonly relativeURL: string;

  constructor(page: Page, relativeURL: string) {
    this.page = page;
    this.relativeURL = relativeURL;
  }

  public async goto(): Promise<void> {
    await this.page.goto(this.relativeURL);
  }
}
