import BasePage from "./base-page";
import { Locator } from "@playwright/test";
import Header from "../components/header";

export default class HomePage extends BasePage {
  readonly header: Header;

  readonly careerButton: Locator;

  readonly navigationMenu: Locator;

  constructor(page, relativeUrl) {
    super(page, relativeUrl);
    this.header = new Header(
      this.page
        .locator("section")
        .filter({ hasText: "About ICT Group Contact Menu" })
        .nth(1)
    );
    this.careerButton = this.page.getByRole("link", { name: "Careers" }).first();
    this.navigationMenu = this.page.getByRole("navigation", { name: "Menu" }).nth(1);
  }

  async openCareerPage() {
    await this.careerButton.click();
  }
}
