import { BrowserContext, Locator, Page } from "@playwright/test";

export async function tearDown(context: BrowserContext): Promise<void> {
  await context.addInitScript(() => {
    localStorage.clear();
  });
  await context.clearCookies();
  await context.close();
}

export async function hoverNavigationMenu(page: Page, navigationMenu: string) {
  await page.getByRole("link", { name: navigationMenu }).hover({ force: true });
}

export async function clickNavigationMenu(page: Page, option: string) {
  await page.getByRole("link", { name: option }).click();
}

export async function clickGetInTouchButton(page: Page) {
  await page.getByRole("link", { name: "Get in touch" }).click();
}

export async function clearFieldAndEnterSequentially(locator: Locator, value: string) {
  await locator.fill("");
  await locator.pressSequentially(value);
}

export async function generateRandomName() {
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  let result = "";
  const length = 7;
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    result += characters[randomIndex];
  }
  return result;
}

export async function generateRandomEmail() {
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  let email = "";
  const length = 10;
  for (let i = 0; i < length; i += 1) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    email += characters[randomIndex];
  }
  email += "@domain.com";
  return email;
}

export async function fillGlobalSearchField(locator: Locator, searchValue: string) {
  await clearFieldAndEnterSequentially(locator, searchValue);
}
