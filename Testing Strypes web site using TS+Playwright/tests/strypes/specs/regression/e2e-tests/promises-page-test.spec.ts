import test from "../../../fixtures/test.fixtures";
import {
  clickNavigationMenu,
  hoverNavigationMenu,
  tearDown,
} from "../../../utils/common";
import CONSTANTS from "../../../constants/constants";
import { expect } from "@playwright/test";

test.describe("Promises Page", async () => {
  test.beforeEach(async ({ homePage }) => {
    await homePage.goto();
  });

  test.afterEach(async ({ context }) => {
    await tearDown(context);
  });

  test(
    "Verify User Could Go to Promises Page From Navigation Menu",
    { tag: ["@E2E-Tests", "@Promises"] },
    async ({ page, homePage, promisesPage }) => {
      await homePage.navigationMenu.isVisible();
      await hoverNavigationMenu(page, CONSTANTS.NAVIGATION_OPTION.about);
      await clickNavigationMenu(
        page,
        CONSTANTS.NAVIGATION_DROP_DOWN_OPTION.ABOUT.our_promises
      );
      expect(await promisesPage.getHeadingTitle()).toEqual(
        CONSTANTS.PROMISES_PAGE_CONTENT.heading_title
      );
    }
  );
});
