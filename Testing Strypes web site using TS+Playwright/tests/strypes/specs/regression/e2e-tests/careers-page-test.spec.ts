import test from "../../../fixtures/test.fixtures";
import { expect } from "@playwright/test";
import CONSTANTS from "../../../constants/constants";
import { tearDown } from "../../../utils/common";

test.describe("Career Page Functionality", async () => {
  test.beforeEach(async ({ homePage }) => {
    await homePage.goto();
    await homePage.openCareerPage();
  });

  test.afterEach(async ({ context }) => {
    await tearDown(context);
  });

  test(
    "Verify Career Page Components",
    { tag: ["@E2E-Tests", "@CareerPage"] },
    async ({ careerPage }) => {
      await test.step("Heading Content", async () => {
        await expect
          .soft(await careerPage.headingTitle.innerText())
          .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.heading_title);
        await expect
          .soft(await careerPage.headingDescription.innerText())
          .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.heading_description);

        await test.step("Great Place to Work Content", async () => {
          await expect
            .soft(await careerPage.greatPlaceToWorkDescription.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.great_place_to_work_description);
        });

        await test.step("Challenge Content", async () => {
          await expect
            .soft(await careerPage.challengeContentTitle.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.challenge_title);
          await expect
            .soft(await careerPage.challengeContentDescription.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.challenge_description);
        });

        await test.step("Hiring Content", async () => {
          await expect(await careerPage.hireTitle).toBeVisible();
          await expect(await careerPage.hireSubTitle).toBeVisible();
          await expect(await careerPage.jobSection).toBeVisible();
        });

        await test.step("Our Team Content", async () => {
          await expect
            .soft(await careerPage.ourTeamTitle.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.our_team_title);
          await expect
            .soft(await careerPage.ourTeamDescription.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.our_team_description);
          await expect(await careerPage.watchVideoButton).toBeVisible();
        });

        await test.step("Awesome Content", async () => {
          await expect(await careerPage.awesomeSection).toBeVisible();
        });

        await test.step("Location Content", async () => {
          await expect(await careerPage.locationSection).toBeVisible();
        });

        await test.step("Internship Content", async () => {
          await expect
            .soft(await careerPage.internshipTitle.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.internship_title);
          await expect
            .soft(await careerPage.internshipDescription.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.internship_description);
          await expect(await careerPage.internshipWatchVideoButton).toBeVisible();
        });

        await test.step("Start IT Career Content", async () => {
          await expect
            .soft(await careerPage.startCareerTitle.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.start_it_career_title);
          await expect
            .soft(await careerPage.startCareerDescription.innerText())
            .toEqual(CONSTANTS.CAREER_PAGE_CONTENT.start_it_career_description);
          await expect(await careerPage.strypesLabButton).toBeVisible();
        });
      });
    }
  );

  test(
    "Verify User is Able to Open a Job Description",
    { tag: ["@E2E-Tests", "@CareerPage"] },
    async ({ page, careerPage, jobPage }) => {
      await careerPage.chooseOpenPositionFromDropDown("Quality Assurance (1)");
      await page.waitForLoadState("networkidle");
      await careerPage.clickPositionName(CONSTANTS.JOB_CONSTANTS.qa_position_name);

      await page.waitForURL(`**/jobs/automation-quality-assurance-engineer/**`);
      await expect(await jobPage.jobTitle.innerText()).toEqual(
        CONSTANTS.JOB_CONSTANTS.qa_position_header
      );
      await expect(jobPage.companyInfo).toBeVisible();
      await expect(jobPage.applyButton).toBeEnabled();
      await expect(jobPage.compensationSection).toBeVisible();
    }
  );
});
