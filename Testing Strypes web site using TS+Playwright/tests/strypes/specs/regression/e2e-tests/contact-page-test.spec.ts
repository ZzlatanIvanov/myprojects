import test from "../../../fixtures/test.fixtures";
import { clickGetInTouchButton, tearDown } from "../../../utils/common";
import CONSTANTS from "../../../constants/constants";
import { expect } from "@playwright/test";

test.describe("Contact Page", async () => {
  test.beforeEach(async ({ homePage }) => {
    await homePage.goto();
  });

  test.afterEach(async ({ context }) => {
    await tearDown(context);
  });

  test(
    "Verify User Could Send Message To The Contact Form",
    { tag: ["@E2E-Tests", "@Contact"] },
    async ({ page, contactPage }) => {
      await clickGetInTouchButton(page);
      await contactPage.fillAllGetInTouchFields();
      await contactPage.clickTermsOfUseButton();
      await contactPage.clickSendButton();
      expect(await contactPage.thankYouMessage.innerText()).toEqual(
        CONSTANTS.CONTACT_PAGE_CONTENT.thank_you_message
      );
    }
  );
});
