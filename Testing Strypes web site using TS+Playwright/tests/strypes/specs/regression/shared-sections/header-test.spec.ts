import test from "../../../fixtures/test.fixtures";
import { fillGlobalSearchField, tearDown } from "../../../utils/common";
import CONSTANTS from "../../../constants/constants";
import { expect } from "@playwright/test";

test.describe("Header Section", async () => {
  test.beforeEach(async ({ homePage }) => {
    await homePage.goto();
  });

  test.afterEach(async ({ context }) => {
    await tearDown(context);
  });

  test(
    "Verify User Could Use The Global Search",
    { tag: ["@E2E-Tests", "@Header"] },
    async ({ page, homePage }) => {
      await homePage.header.clickSearchButton();
      const locator = page.getByRole("searchbox", { name: "Search Search" });
      await fillGlobalSearchField(locator, CONSTANTS.GLOBAL_SEARCH.nearsurance);
      await page.keyboard.press("Enter");

      await expect(
        page.getByRole("heading", { name: "Results for: Nearsurance" })
      ).toBeVisible();
      await expect(
        page.locator("article").filter({ hasText: "Nearsurance Nearsurance" })
      ).toBeVisible();
    }
  );
});
