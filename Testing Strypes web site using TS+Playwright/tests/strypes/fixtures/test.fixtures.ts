import { test as base } from "@playwright/test";
import HomePage from "../pages/home-page";
import CareerPage from "../pages/career-page";
import { StrypesRelativeURLs } from "../constants/routes";
import JobPage from "../pages/job-page";
import PromisesPage from "../pages/promises-page";
import ContactPage from "../pages/contact-page";

const test = base.extend<{
  homePage: HomePage;
  careerPage: CareerPage;
  jobPage: JobPage;
  promisesPage: PromisesPage;
  contactPage: ContactPage;
}>({
  homePage: async ({ page }, use) => {
    await use(new HomePage(page, ""));
  },
  careerPage: async ({ page }, use) => {
    await use(new CareerPage(page, StrypesRelativeURLs.CAREERS));
  },
  jobPage: async ({ page }, use) => {
    await use(new JobPage(page, StrypesRelativeURLs.JOBS));
  },
  promisesPage: async ({ page }, use) => {
    await use(new PromisesPage(page, StrypesRelativeURLs.PROMISES));
  },
  contactPage: async ({ page }, use) => {
    await use(new ContactPage(page, StrypesRelativeURLs.CONTACT));
  },
});
export default test;
