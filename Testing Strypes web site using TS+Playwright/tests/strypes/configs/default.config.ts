import { config } from "dotenv";

import type { PlaywrightTestConfig } from "@playwright/test";

config();

const BASE_URL = process.env.PLAYWRIGHT_BASE_URL;
const defaultConfig: PlaywrightTestConfig = {
  timeout: 120000,
  expect: {
    timeout: 60000,
  },
  forbidOnly: true,
  fullyParallel: true,
  retries: 0,
  workers: 2,
  testDir: "../specs",
  use: {
    baseURL: BASE_URL,
    headless: false,
    video: "off",
    trace: "on",
    screenshot: "off",
  },
  projects: [
    {
      name: "Desktop Chromium",
      use: {
        browserName: "chromium",
        viewport: { width: 1920, height: 1080 },
        ignoreHTTPSErrors: true,
      },
    },
  ],
};

export default defaultConfig;
