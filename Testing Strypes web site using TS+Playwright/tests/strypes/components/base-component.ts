import { type Locator } from "@playwright/test";

export default abstract class BaseComponent {
  constructor(protected readonly container: Locator) {}
}
