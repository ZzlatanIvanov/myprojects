import BaseComponent from "./base-component";
import { Locator } from "@playwright/test";

export default class Header extends BaseComponent {
  readonly searchButton: Locator;

  constructor(container: Locator) {
    super(container);
    this.searchButton = container.getByRole("button", { name: "Search" });
  }

  async clickSearchButton() {
    await this.searchButton.click();
  }
}
