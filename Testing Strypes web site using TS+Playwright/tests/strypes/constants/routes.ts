export const Strypes = {
  MAIN_URL: "https://ict-strypes.eu",
};

export const StrypesRelativeURLs = {
  CAREERS: `/careers`,
  JOBS: `/jobs`,
  PROMISES: `/promises`,
  CONTACT: `/contact`,
};
