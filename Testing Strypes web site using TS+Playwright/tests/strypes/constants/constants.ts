const CONSTANTS = {
  JOB_CONSTANTS: {
    qa_position_name: "Automation Quality Assurance",
    qa_position_header: "Automation Quality Assurance Engineer",
  },
  NAVIGATION_OPTION: {
    about: "About ",
  },
  NAVIGATION_DROP_DOWN_OPTION: {
    ABOUT: {
      our_promises: "Our promises",
    },
  },
  PROMISES_PAGE_CONTENT: {
    heading_title: "Our promises",
  },
  CONTACT_PAGE_CONTENT: {
    thank_you_message: "Thank you for submitting the form.",
  },
  GLOBAL_SEARCH: {
    nearsurance: "Nearsurance",
  },
  CAREER_PAGE_CONTENT: {
    heading_title: "We are ICT Strypes -\nwe do awesome things",
    heading_description:
      "We want our team to feel inspired and love what they do. Passion for technology is what connects us, in combination with the drive to constantly develop our skills, to grow and to have fun together. Just like a real family. .",
    great_place_to_work_description:
      "We are officially recognized by the Great Place to Work certification: an achievement that reflects our continuous pursuit to create an environment where individuals thrive, feel valued and are encouraged to be themselves while contributing to the company’s success and growth.",
    challenge_title: "Take on our Challenge!",
    challenge_description:
      "Grab the opportunity to win an award and join our awesome team! Programming is a skill many have these days. Turning it into something fun and entertaining is another thing. So if you are a Programming enthusiast, Software developer, or a wanna-be pirate, read more about our Liar's Dice and take on this challenge!",
    our_team_title: "Our awesome team",
    our_team_description:
      "What makes ICT Strypes special are the incredible people who work here. Our company’s culture is what makes us stand out from the crowd. We value teamwork, agility, ingenuity and integrity. We welcome both young IT specialists to help them build a career with us, and experienced developers to bring deep knowledge in the team.",
    internship_title: "Internship at ICT Strypes",
    internship_description:
      "At ICT Strypes we are determined to create more opportunities for young motivated people who are interested in cutting-edge technologies.\nWe welcome specialists with basic programming knowledge who are ready to learn and grow in the field.",
    start_it_career_title: "Do you think about starting a career in IT?",
    start_it_career_description:
      "Strypes Lab 2023 is our main training initiative that aims to upskill the technical knowledge of young professionals in Bulgaria.\nStrypes Lab offers courses on C, C++, Python and we have a brand you edition for Feature Integrators. \nCheck our upcoming courses here:",
  },
};
export default CONSTANTS;
