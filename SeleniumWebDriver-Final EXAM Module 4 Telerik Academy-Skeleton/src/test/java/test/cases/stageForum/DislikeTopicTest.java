package test.cases.stageForum;

import org.junit.Test;
import pages.forum.CertainPage;
import test.cases.BaseTestSetup;

public class DislikeTopicTest extends BaseTestSetup {


    @Test
    public void dislikeTopic() {
        login();
        CertainPage certainPage = new CertainPage(actions.getDriver());
        certainPage.dislikeCertainTopic();
    }
}
