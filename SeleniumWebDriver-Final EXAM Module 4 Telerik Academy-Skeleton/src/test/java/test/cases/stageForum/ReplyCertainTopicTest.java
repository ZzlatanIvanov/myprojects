package test.cases.stageForum;

import org.junit.Test;
import pages.forum.CertainPage;
import test.cases.BaseTestSetup;

public class ReplyCertainTopicTest extends BaseTestSetup {

    @Test
    public void replyTopic() {
        login();
        CertainPage certainPage = new CertainPage(actions.getDriver());
        certainPage.clickOnSearchButton();
        certainPage.replyToCertainTopic();
    }
}
