package test.cases.stageForum;

import org.junit.Test;
import pages.forum.MainForumPage;
import test.cases.BaseTestSetup;

public class CreateTopicTest extends BaseTestSetup {

    @Test
    public void createTopic(){

        login();
        MainForumPage mainForumPage = new MainForumPage(actions.getDriver());
        mainForumPage.createNewTopic();
    }
}
