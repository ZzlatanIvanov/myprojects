package pages.forum;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseForumPage {

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser(){
        String username = getConfigPropertyByKey("forum.username");
        String password = getConfigPropertyByKey("forum.password");

        navigateToPage();
        assertNavigatedUrl();

        actions.clickElement("forum.loginButton");
        actions.waitForElementVisible("forum.loginPage.username");
        actions.typeValueInField(username, "forum.loginPage.username");
        actions.typeValueInField(password, "forum.loginPage.password");

        actions.clickElement("forum.loginPage.signInButton");
        actions.assertElementPresent("forum.assertUserLogin");
    }
}
