package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public abstract class BaseForumPage extends BasePage {

    public BaseForumPage(WebDriver driver, String pageUrlKey) {
        super(driver, pageUrlKey);
    }

}
