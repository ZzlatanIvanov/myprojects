package pages.forum;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import static pages.forum.utils.Constants.*;

public class CertainPage extends BaseForumPage {

    public CertainPage(WebDriver driver) {
        super(driver, "forum.certainURL");
    }

    public void clickOnSearchButton() {
        actions.waitForElementVisible("forum.searchButton");
        actions.clickElement("forum.searchButton");
        actions.locateElementAndPressEnter(SEARCH_TOPIC_TEXT,"forum.searchBar");
        actions.waitForElementVisible("forum.clickAtFoundTopic");
        actions.clickElement("forum.clickAtFoundTopic");
    }

    public void replyToCertainTopic(){
        actions.waitForElementVisible("forum.replyButton");
        actions.clickElement("forum.replyButton");
        actions.waitForElementVisible("forum.certainPage.description");
        actions.clickElement("forum.certainPage.description");
        actions.typeValueInField(DESCRIPTION_REPLY_TEXT,"forum.certainPage.description");
        actions.clickElement("forum.certainPage.replyToTopicButton");
//        actions.assertElementPresent();
    }
    public void likeCertainTopic(){
        navigateToPage();
        actions.waitForElementVisible("forum.certainPage.likeButton");
        actions.clickElement("forum.certainPage.likeButton");
        actions.waitForElementVisible("forum.certainPage.assertLike");
        actions.assertElementPresent("forum.certainPage.assertLike");
    }
    public void dislikeCertainTopic(){
        navigateToPage();
        actions.waitForElementVisible("forum.certainPage.likedButton");
        actions.clickElement("forum.certainPage.likedButton");
        actions.assertElementPresent("forum.certainPage.assertDislike");
    }

}
