package pages.forum;

import org.openqa.selenium.WebDriver;

import static pages.forum.utils.Constants.*;

public class MainForumPage extends BaseForumPage {

    public MainForumPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void createNewTopic() {

        actions.clickElement("forum.mainForumPage.createTopicButton");
        actions.waitForElementVisible("forum.mainForumPage.titlePlaceHolder");
        actions.typeValueInField(TITLE_TEXT, "forum.mainForumPage.titlePlaceHolder");

        actions.waitForElementVisible("forum.mainForumPage.descriptionPlaceHolder");
        actions.clickElement("forum.mainForumPage.descriptionPlaceHolder");
        actions.typeValueInField(DESCRIPTION_TEXT, "forum.mainForumPage.descriptionPlaceHolder");
        actions.clickElement("forum.mainForumPage.create");
//        Assert
        actions.waitForElementVisible("forum.mainForumPage.assertName");
        actions.assertElementPresent("forum.mainForumPage.assertName");
    }

}
