package pages.forum.utils;

import org.openqa.selenium.devtools.v101.media.model.Timestamp;

import java.util.Date;
import java.util.Random;

public class Constants {

    static public String randomText(){
        String title;
        Random random = new Random();
        int n = random.nextInt(252);
        n += 3;
        title = String.valueOf(n);
        return title;
    }

    static Date date = new Date();
    // getting the object of the Timestamp class
    static Timestamp timestamp = new Timestamp(date.getTime());
    public static final String TITLE_TEXT = "Test create my new topic "+ randomText();
    public static final String DESCRIPTION_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at " +
            "erat eget tellus iaculis fdffrf ge " + randomText();
    public static final String SEARCH_TOPIC_TEXT = "Alpha 38 QA - We are awesome and great";

    public static final String DESCRIPTION_REPLY_TEXT = "> Hi, I am Zlatan, and I did it :grinning: :partying_face: :beers: " +
            ":see_no_evil: :hear_no_evil: :speak_no_evil: :heart: ";


}
