**Instructions**
1. Clone repository
2. Open `SeleniumWebDriver-EXAM-Skeleton[test-automation-framework]` as a IntelliJ IDEA Project
3. Build
4. Run tests in logical order

TASKS

Use Telerik Academy Staging Forum (https://stage-forum.telerikacademy.com/) to practice testing with Selenium WebDriver and the provided Test Automation Framework. 

Java 11 is the mandatory version, otherwise, your project could not be built, run and assessed properly. 

Task 1

Create a new topic in the Staging forum via the UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices in creating a topic
    The title should be unique 
    The title should be between 5 and 255 symbols
    Title length per each test run shouldn't be fixed like the Title is always 25 or 30 symbols
    The description should be unique
    The description should be at least 100 symbols
    Description length per each test run shouldn't be fixed like the Description is always 100 or 105 symbols
    The test should be run more than 5 times without errors
    Testing formatting or other features is optional
    Validate the topic creation
    Delete topic is optional. You can do it immediately after creation or via a separate test.

Task 2

Comment on the certain topic with the title "Alpha 38 QA - We are awesome and great"   in Staging Forum via the UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices
    Comment should be quoted, and contains text and emoticons exact as in the screenshot below and your name
    Validate the comment creation

Task 3

Like the certain topic with the title "Alpha 38 QA - We are awesome and great" in Staging Forum via the UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices
    Like the topic. The screenshot is illustrative, and you should use the mentioned topic above 
    The test should be run unlimited times without errors
    Validate topic is liked

Task 4

Dislike the certain topic with the title "Alpha 38 QA - We are awesome and great" in Staging Forum via the UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices
    Dislike the topic. You should use the mentioned topic above
    The test should be run unlimited times without errors
    Validate topic is disliked


Hints and what don't forget to have:

DELETE YOUR CREDENTIALS BEFORE SUBMITTING THE SOLUTION!!!

Follow the good testing and coding practices and consider how to handle and go through the forum validations.

Please finally, run one like topic test :)