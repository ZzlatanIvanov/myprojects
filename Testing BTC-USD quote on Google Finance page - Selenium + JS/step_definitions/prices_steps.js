// features/step_definitions/prices_steps.js
import { Given, Then, When } from "@cucumber/cucumber";
import Utils from "../support/utils.mjs";
import { expect } from "chai";

// This can be also done using CustomWorld like in the API tests project
let initialPrice;
let averagePrice;
let prices = [];

Given(
  "user navigates to Real-time Quote {string} - Google Finance page",
  async function (symbol) {
    await this.financePage.navigate(symbol);
    await this.financePage.rejectAllCookieButton();
  },
);

When(
  "gets the initial and average quote price within duration of {int} minutes and poll interval {int} seconds",
  async function (collectDuration, pollInterval) {
    initialPrice = await this.financePage.getQuotePrice(
      this.financePage.quotePriceSelector,
    );
    [averagePrice] =
      await this.financePage.getIndividualAndAverageQuotePriceForSetTime(
        pollInterval,
        collectDuration,
      );
  },
);

When(
  "gets the initial and all quote price within duration of {int} minutes and poll interval {int} seconds",
  async function (collectDuration, pollInterval) {
    initialPrice = await this.financePage.getQuotePrice(
      this.financePage.quotePriceSelector,
    );
    [, prices] =
      await this.financePage.getIndividualAndAverageQuotePriceForSetTime(
        pollInterval,
        collectDuration,
      );
  },
);

Then(
  "the average quote price does not vary by more than {int} percentage",
  async function (threshold) {
    const percentageDifferance = Utils.getPercentDiff(
      initialPrice,
      averagePrice,
    );
    await expect(threshold).to.be.greaterThan(percentageDifferance);
  },
);

Then(
  "the individual quote prices does not vary by more than {int} percentage",
  async function (threshold) {
    for (let price of prices) {
      const percentageDifferance = Utils.getPercentDiff(initialPrice, price);
      await expect(threshold).to.be.greaterThan(percentageDifferance);
    }
  },
);
