Feature: Create tests here

  Scenario Outline: Average BTC-USD price in time interval <minutes> minute does not vary more than 1% compared to the initial value
    Given user navigates to Real-time Quote "BTC-USD" - Google Finance page
    When gets the initial and average quote price within duration of <minutes> minutes and poll interval 10 seconds
    Then the average quote price does not vary by more than <percentage> percentage
    Examples:
      | minutes |percentage|
      |    1    |    1     |
      |    3    |    1     |
      |    5    |    1     |

  Scenario Outline: No individual quote BTC-USD reading within the time interval <minutes> varies by more than 2% from the initial recorded value
    Given user navigates to Real-time Quote "BTC-USD" - Google Finance page
    When gets the initial and all quote price within duration of <minutes> minutes and poll interval 10 seconds
    Then the individual quote prices does not vary by more than <percentage> percentage
    Examples:
      | minutes |percentage|
      |    1    |    2     |
      |    3    |    2     |
      |    5    |    2     |