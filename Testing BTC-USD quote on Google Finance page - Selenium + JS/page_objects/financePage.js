import { By, WebDriver } from "selenium-webdriver";
import { BASE_URL } from "../config.js";

/**
 * Represents the Finance Page object.
 */
class FinancePage {
  /**
   * Creates an instance of FinancePage.
   * @param {WebDriver} driver - The WebDriver instance.
   */
  constructor(driver) {
    this.driver = driver;
    this.rejectAllCookieButtonSelector = By.css(
      'button[aria-label="Reject all"]',
    );
    this.quotePriceSelector = By.xpath('//div[@class="YMlKec fxKbKc"]');
  }

  async navigate(symbol) {
    await this.driver.navigate().to(`${BASE_URL}/${symbol}`);
  }

  async rejectAllCookieButton() {
    await this.driver.findElement(this.rejectAllCookieButtonSelector).click();
  }

  async getIndividualAndAverageQuotePriceForSetTime(
    pollIntervalSeconds,
    collectDurationMinutes,
  ) {
    const numberOfMeasurementsInMinute =
      (collectDurationMinutes * 60) / pollIntervalSeconds;
    let totalPrice = 0;
    let allMeasurementsArray = [];

    for (let i = 0; i < numberOfMeasurementsInMinute; i++) {
      let quotePrice = await this.getQuotePrice(this.quotePriceSelector);
      allMeasurementsArray.push(quotePrice);
      totalPrice = totalPrice + quotePrice;
      await this.driver.sleep(pollIntervalSeconds * 1000);
    }
    const averagePrice = totalPrice / numberOfMeasurementsInMinute;
    return [averagePrice, allMeasurementsArray];
  }

  async getQuotePrice(selector) {
    let currentPrice = await this.driver.findElement(selector).getText();
    return Number(currentPrice.replaceAll(",", ""));
  }
}

export default FinancePage;
