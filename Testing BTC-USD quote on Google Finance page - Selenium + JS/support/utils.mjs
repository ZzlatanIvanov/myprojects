export default class Utils {
  /**
   * Calculates the percentage difference between two numbers.
   * @param {number} oldValue - The old value.
   * @param {number} newValue - The new value.
   * @returns {number} The percentage difference.
   */
  static getPercentDiff = (oldValue, newValue) => {
    let difference;
    if (oldValue >= newValue) {
      // decreased price
      difference = ((oldValue - newValue) / oldValue) * 100;
    } else {
      //  increased price
      difference = ((newValue - oldValue) / oldValue) * 100;
    }
    return difference;
  };
}
