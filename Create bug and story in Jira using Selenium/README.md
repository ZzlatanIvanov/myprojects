**Instructions**
1. Clone repository
2. Open `aphaframework/testframework` as a IntelliJ IDEA Project
3. Build
4. Run tests in `src\test\java\testCases\`

TASKS

Use your free JIRA projects to practice testing with Selenium WebDriver and the provided Test Automation Framework. 

Java 11 is the recommended version, otherwise, your project could not be built, run and assessed properly. 

Task 1

Create a story in JIRA via JIRA UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices in creating a story
    Set priority based on the severity
    Validate the story creation

Task 2

Create a bug in JIRA via JIRA UI and Selenium Test Automation Framework (Page Object Model)

    Follow the best practices in creating a bug
    Set priority based on the severity
    Validate the bug creation

Task 3

Link the bug to the story in JIRA via JIRA UI and Selenium Test Automation Framework (Page Object Model)

    Use relation 'is blocked by'
    Validate proper relation exists


