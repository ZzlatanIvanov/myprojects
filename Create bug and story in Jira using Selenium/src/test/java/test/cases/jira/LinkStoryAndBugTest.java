package test.cases.jira;
import org.junit.Test;
import pages.jira.*;

import static com.telerikacademy.testframework.Constants.*;
import static com.telerikacademy.testframework.Constants.STEPS_TO_REPRODUCE_BUG;
import static java.lang.String.format;
import static org.junit.Assert.assertTrue;

public class LinkStoryAndBugTest extends BaseTest {


    @Test
    public void linkStoryAndBug() {

//  Create Story
        LoginPageJira loginPage = new LoginPageJira(actions.getDriver());
        loginPage.loginUser("user");

        StartPage startPage = new StartPage(actions.getDriver());
        startPage.clickOnProduct("jira.startPage.products");

        ProjectsPage projectsPage = new ProjectsPage(actions.getDriver());
        projectsPage.clickOnProject("jira.projectPage");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.clickOnCreateButton("jira.boardPage.create");
        boardPage.chooseIssueTypeStory();
        boardPage.choosePriority();

        boardPage.typeSummaryAndDescription(SUMMARY, STEPS_TO_REPRODUCE_STORY);
        boardPage.clickCreateIssueButton();
        String createdStoryID = boardPage.getCreatedIssueID();

//   Create Bug
        boardPage.clickOnCreateButton("jira.boardPage.create");
        boardPage.chooseIssueTypeBug();
        boardPage.choosePriority();

        boardPage.typeSummaryAndDescription(SUMMARY_BUG,STEPS_TO_REPRODUCE_BUG);
        boardPage.clickCreateIssueButton();
        boardPage.clickViewIssueAnchorElement();

// Link Story to the Bug
        actions.waitForElementVisible("jira.backlog.linkIssueButton");
        actions.clickElement("jira.backlog.linkIssueButton");

        actions.waitForElementVisible("jira.backlogPage.containerDropDown");
        actions.clickElement("jira.backlogPage.containerDropDown");
        actions.clickElement("jira.backlogPage.blocks");
        actions.waitFor(2000);
        actions.clickElement("jira.backlogPage.searchIssuesDropDown");

        actions.locateElementAndPressEnter(createdStoryID);
        actions.waitFor(2000);

        actions.clickElement("jira.backlogPage.linkButton");
        assertTrue(format("Story with ID %s is linked as blocked by current bug ",createdStoryID)
                ,actions.isElementVisible("jira.blockedIssueIsLinkedByID",createdStoryID));
    }
}
