package test.cases.jira;

import org.junit.Test;
import pages.jira.*;

import static com.telerikacademy.testframework.Constants.*;


public class CreateStoryTest extends BaseTest {

    @Test
    public void createStory() {

        LoginPageJira loginPage = new LoginPageJira(actions.getDriver());
        loginPage.loginUser("user");
        loginPage.assertLogin();

        StartPage startPage = new StartPage(actions.getDriver());
        startPage.clickOnProduct("jira.startPage.products");

        ProjectsPage projectsPage = new ProjectsPage(actions.getDriver());
        projectsPage.clickOnProject("jira.projectPage");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.clickOnCreateButton("jira.boardPage.create");
        boardPage.chooseIssueTypeStory();
        boardPage.choosePriority();

        boardPage.typeSummaryAndDescription(SUMMARY, STEPS_TO_REPRODUCE_STORY);
        boardPage.clickCreateIssueButton();
        boardPage.validateIssueCreation();

    }
}
