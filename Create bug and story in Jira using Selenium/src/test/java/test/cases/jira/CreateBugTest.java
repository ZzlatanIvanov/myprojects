package test.cases.jira;

import org.junit.Test;
import pages.jira.*;

import static com.telerikacademy.testframework.Constants.STEPS_TO_REPRODUCE_BUG;
import static com.telerikacademy.testframework.Constants.SUMMARY_BUG;

public class CreateBugTest extends BaseTest {

    @Test
    public void createBug(){

        LoginPageJira loginPage = new LoginPageJira(actions.getDriver());
        loginPage.loginUser("user");
        loginPage.assertLogin();

        StartPage startPage = new StartPage(actions.getDriver());
        startPage.clickOnProduct("jira.startPage.products");

        ProjectsPage projectsPage = new ProjectsPage(actions.getDriver());
        projectsPage.clickOnProject("jira.projectPage");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.clickOnCreateButton("jira.boardPage.create");
        boardPage.chooseIssueTypeBug();
        boardPage.choosePriority();

        boardPage.typeSummaryAndDescription(SUMMARY_BUG,STEPS_TO_REPRODUCE_BUG);
        boardPage.clickCreateIssueButton();
        boardPage.validateIssueCreation();

    }
}
