package com.telerikacademy.testframework;

public class Constants {

    public static final String USER_NAME = "ZLATAN IVANOV";
    public static String SUMMARY = "Change language from English to German, when click at German";
    public static final String STEPS_TO_REPRODUCE_STORY = "Narrative:\n" +
            "In order to read in my own language\n" +
            "As a client of this site\n" +
            "I want to be able to change the language\n" +
            "Steps to reproduce:\n" +
            "1.Enter URL in the browser https://www.phptravels.net/en \n" +
            "2.Click ENGLISH button\n" +
            "3.Select German\n" +
            "\n" +
            "Expected result:\n" +
            "Page reload and all its content is in German";


    public static String SUMMARY_BUG = "Language is not changed from English to German, when click at German";

    public static final String STEPS_TO_REPRODUCE_BUG = "Narrative:\n" +
            "In order to read in my own language\n" +
            "As a client of this site\n" +
            "I want to be able to change the language\n" +
            "Steps to reproduce:\n" +
            "1.Enter URL in the browser https://www.phptravels.net/en \n" +
            "2.Click ENGLISH button\n" +
            "3.Select German\n" +
            "\n" +
            "Expected result:\n" +
            "Page reload and all its content is in German\n" +
            "Actual result:\n" +
            "Page is redirecting to https://www.phptravels.net/de,  but the content is still in English";
}

