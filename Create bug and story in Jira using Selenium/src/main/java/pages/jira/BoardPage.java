package pages.jira;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

public class BoardPage extends BaseJiraPage {

    public BoardPage(WebDriver driver) {
        super(driver, "atlassian.boardPage");
    }

    public void clickOnCreateButton(String projectName) {
        actions.waitFor(5000);
        actions.waitForElementVisible("jira.boardPage.create");
        actions.clickElement("jira.boardPage.create", projectName);
    }

    public void typeSummaryAndDescription(String summary, String description) {
        actions.waitFor(2000);
        actions.clickElement("jira.boardPage.summaryField");
        actions.typeValueInField(summary, "jira.boardPage.summaryField");
        actions.clickElement("jira.boardPage.descriptionField");
        actions.typeValueInField(description, "jira.boardPage.descriptionField");
    }

    public void clickCreateIssueButton() {
        actions.waitForElementClickable("//button[@class='css-goggrm']", 5);
        actions.clickElement("//button[@class='css-goggrm']");
    }

    public void chooseIssueTypeStory() {
        actions.waitFor(1000);
        actions.clickElement("jira.boardPage.chooseIssueType");
        actions.clickElement("jira.boardPage.chooseIssueTypeStory");
    }

    public void chooseIssueTypeBug() {
        actions.waitFor(1000);
        actions.clickElement("jira.boardPage.chooseIssueType");
        actions.clickElement("jira.boardPage.chooseIssueTypeBug");
    }

    public void choosePriorityFromLowToHigh() {
        actions.waitFor(2000);
        actions.clickElement("jira.boardPage.choosePriorityLow");
        actions.clickElement("jira.boardPage.choosePriorityHigh");
    }

    public void choosePriorityFromMediumToHigh() {
        actions.waitFor(2000);
        actions.clickElement("jira.boardPage.choosePriorityMedium");
        actions.clickElement("jira.boardPage.choosePriorityHigh");
    }

    public void validateIssueCreation() {
        actions.waitFor(2000);
        actions.assertElementPresent("jira.boardPage.issueSuccessfulCreation");
    }

    public void choosePriority() {
        actions.waitFor(3000);
        actions.clickElement("((//div[@id='priority-container'])//div)[5]");
        actions.waitFor(2000);
        actions.clickElement("jira.boardPage.choosePriorityHigh");
    }

    public void clickViewIssueAnchorElement() {
        actions.waitForElementVisible("jira.board.viewIssuePage");
        actions.clickElement("jira.board.viewIssuePage");
    }

    public String getCreatedIssueID() {
        actions.waitForElementVisible("jira.board.successfulCreationIssueMessage");
        return StringUtils.substringBetween(actions
                        .getElementText("jira.board.successfulCreationIssueMessage")
                , "You've created \"", "\" issue.");
    }
}
