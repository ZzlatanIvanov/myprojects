package pages.jira;

import org.openqa.selenium.WebDriver;

public class StartPage extends BaseJiraPage {

    public StartPage(WebDriver driver) {
        super(driver, "atlassian.startUrl");
    }

    public void clickOnProduct(String productName) {
        actions.waitFor(5000);
        actions.waitForElementVisible("jira.startPage.products");
        actions.clickElement("jira.startPage.products", productName);
    }

}
