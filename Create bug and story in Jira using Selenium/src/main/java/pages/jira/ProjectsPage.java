package pages.jira;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class ProjectsPage  extends BaseJiraPage{

    public ProjectsPage(WebDriver driver) {
        super(driver, "atlassian.projectsPage");
    }

    public void clickOnProject(String projectName){
        actions.waitForElementVisible("jira.projectPage");
        actions.clickElement("jira.projectPage", projectName);
    }
}
