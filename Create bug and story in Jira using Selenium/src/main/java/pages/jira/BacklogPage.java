package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.UserActions.loadBrowser;

public class BacklogPage extends BaseJiraPage {

    public BacklogPage(WebDriver driver) {
        super(driver, "atlassian.backlogBrowse.issue");
    }

    public void linkIssue(String bugKey) {

        navigateToPage();
        actions.waitForElementVisible("jira.backlog.linkIssueButton");
        actions.clickElement("jira.backlog.linkIssueButton");

        actions.waitForElementVisible("jira.backlogPage.isBlockedBy");
        actions.clickElement("jira.backlogPage.isBlockedBy");
        actions.waitFor(2000);
        actions.clickElement("jira.backlogPage.searchIssuesDropDown");

//        actions.typeValueInField(bugKey, "jira.backlogPage.searchIssuesDropDown");
//        actions.waitFor(2000);

        actions.locateElementAndPressEnter(bugKey);

        actions.waitFor(2000);
        actions.clickElement("jira.backlogPage.linkButton");
    }
}
