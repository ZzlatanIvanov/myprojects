package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Constants.USER_NAME;

public class LoginPageJira extends BaseJiraPage {

    public LoginPageJira(WebDriver driver) {
        super(driver, "atlassian.base.url");
    }

    public void loginUser(String userKey) {

        String username = Utils.getConfigPropertyByKey("atlassian.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("atlassian.users." + userKey + ".password");

        navigateToPage();
        actions.clickElement("jira.myAccount.button");
        actions.clickElement("jira.login.button");

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.submitButton");

        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.submitButton");
    }

    public void assertLogin() {
        actions.waitForElementClickable("//button[@data-testid='nav__profile-menu-trigger']", 10);
        actions.clickElement("//button[@data-testid='nav__profile-menu-trigger']");
        Assert.assertEquals(String.format("%s is expected", USER_NAME), driver.findElement(By.cssSelector("div.css-1oi01z"))
                .getText(), USER_NAME);
    }
}
