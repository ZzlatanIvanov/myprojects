package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    protected static final String ERROR_MESSAGE_NULL = "Can not be null!";
    private List<Comment> comments = new ArrayList<>();
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;

    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.vehicleType = vehicleType;
    }


    protected String errorMessage(String text, int min, int max) {
        return String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, text, min, max);
    }

    private void setMake(String make) {
        Validator.ValidateNull(make, "Make " + ERROR_MESSAGE_NULL);
        Validator.ValidateIntRange(make.length(), ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH,
                errorMessage("Make", ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH));
        this.make = make;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model, "Model " + ERROR_MESSAGE_NULL);
        Validator.ValidateIntRange(model.length(), ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH,
                errorMessage("Model", ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH));
        this.model = model;
    }

    private void setPrice(double price) {
        Validator.ValidateDecimalRange(price, ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE,
                String.format("Price must be between %.1f and %.1f!", ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE));
        this.price = price;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        builder.append(String.format("  Make: %s", getMake())).append(System.lineSeparator());
        builder.append(String.format("  Model: %s",getModel())).append(System.lineSeparator());
        builder.append(String.format("  Wheels: %d", vehicleType.getWheelsFromType())).append(System.lineSeparator());
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //    this method can be access in all subclasses and could be Overridden
    protected abstract String printAdditionalInfo();


    private String printComments() {
        StringBuilder builder = new StringBuilder();
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

}
