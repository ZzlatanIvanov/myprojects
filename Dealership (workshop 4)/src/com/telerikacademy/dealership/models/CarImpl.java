package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.List;

public class CarImpl extends VehicleBase implements Car {

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    @Override
    protected String errorMessage(String text, int min, int max) {
        return String.format(text + " must be between %d and %d!", min, max);
    }

    public void setSeats(int seats) {
        Validator.ValidateIntRange(seats, ModelsConstants.MIN_SEATS, ModelsConstants.MAX_SEATS,
                errorMessage("Seats", ModelsConstants.MIN_SEATS, ModelsConstants.MAX_SEATS));
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %s", seats);
    }
}
