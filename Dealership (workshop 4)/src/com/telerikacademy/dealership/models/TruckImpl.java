package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private static final int MAX_WEIGHT = 100;
    private static final int MIN_WEIGHT = 1;
    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    protected String errorMessage(String text, int min, int max) {
        return String.format(text + " capacity must be between %d and %d!", min, max);
    }

    public void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY,
                errorMessage("Weight", ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %dt", weightCapacity);
    }
}
