package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    public void setCategory(String category) {
        Validator.ValidateNull(category, "Category " + ERROR_MESSAGE_NULL);
        Validator.ValidateIntRange(category.length(), ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH,
                errorMessage("Category", ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH));
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Category: %s", category);
    }
}
